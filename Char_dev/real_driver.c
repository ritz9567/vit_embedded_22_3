#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/module.h>
#include<linux/kdev_t.h>
#include<linux/device.h>
#include<linux/cdev.h>
#include<linux/fs.h>
#include<linux/slab.h>    //for  kmalloc
#include<linux/uaccess.h> //copy_to/from_usr

#define mem_size 1024

dev_t dev = 0;

static struct class *real_dev_class;
static struct cdev real_cdev;
uint8_t *ker_buf;

static int __init real_init(void);
static void __exit real_exit(void);


static int real_open(struct inode *inode,struct file *file);
static int real_release(struct inode *inode,struct file *file);
static ssize_t real_read(struct file *file,char __user *buf,size_t len,loff_t *off);
static ssize_t real_write(struct file *file,const char *buf,size_t len,loff_t *off);


static struct file_operations real_fops = {
.owner 		= THIS_MODULE,
.read  		= real_read,
.write		= real_write,
.open		=  real_open,
.release	=  real_release,
};


static int  real_open(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "Dummy driver open function called..\n");
	if((ker_buf = kmalloc(mem_size,GFP_KERNEL)) == 0){
		printk(KERN_INFO "memory_allocation failed ...\n");
		return -1;
	}
	return 0;
}

static int real_release(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "Dummy driver release function called..\n");
	kfree(ker_buf);
	return 0;
}

static ssize_t real_read(struct file *file,char __user *buf,size_t len,loff_t *off)
{
	printk(KERN_INFO "Dummy driver read function called..\n");
	copy_to_user(buf,ker_buf,mem_size);
	return mem_size;
}

static ssize_t real_write(struct file *file,const char *buf,size_t len,loff_t *off)
{
	printk(KERN_INFO "Dummy driver write function called..\n");
	copy_from_user(ker_buf,buf,len);
	return len;
}



static int __init real_init(void)
{
	printk(KERN_INFO " calling init function \n");	
	if((alloc_chrdev_region(&dev,0,1,"real_dev")) < 0){
		printk(KERN_INFO "cannot allocate major number\n");
	}

	/*creating cdev structure */
	
	cdev_init(&real_cdev,&real_fops);
	real_cdev.owner = THIS_MODULE;
	real_cdev.ops   = &real_fops;

	if((cdev_add(&real_cdev,dev,1)) < 0){
		printk(KERN_INFO " cannot add device to the system\n");	
		goto r_class;
	}

	if((real_dev_class = class_create(THIS_MODULE,"real_class")) < 0){	
		printk(KERN_INFO " cannot add device to the system\n");	
		goto r_class;
	}	
	
	if((device_create(real_dev_class,NULL,dev,NULL,"real_device")) < 0){
		printk(KERN_INFO " failed to create device \n");	
		goto r_device;
	}
	
	return 0;
r_device:
	class_destroy(real_dev_class);

r_class:
	unregister_chrdev_region(dev,1);
	return -1;

}


static void __exit real_exit(void)
{

	printk(KERN_INFO " calling exit function \n");	
	device_destroy(real_dev_class,dev);
	class_destroy(real_dev_class);
	cdev_del(&real_cdev);
	unregister_chrdev_region(dev,1);

}



module_init(real_init);
module_exit(real_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sailesh kumar");
MODULE_DESCRIPTION("A real char driver");
MODULE_VERSION("2.0");

