#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/module.h>
#include<linux/kdev_t.h>
#include<linux/device.h>
#include<linux/cdev.h>
#include<linux/fs.h>


dev_t dev = 0;

static struct class *dummy_dev_class;
static struct cdev dummy_cdev;

static int __init dummy_init(void);
static void __exit dummy_exit(void);


static int dummy_open(struct inode *inode,struct file *file);
static int dummy_release(struct inode *inode,struct file *file);
static ssize_t dummy_read(struct file *file,char __user *buf,size_t len,loff_t *off);
static ssize_t dummy_write(struct file *file,const char *buf,size_t len,loff_t *off);


static struct file_operations dummy_fops = {
.owner 		= THIS_MODULE,
.read  		= dummy_read,
.write		= dummy_write,
.open		= dummy_open,
.release	= dummy_release,
};


static int dummy_open(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "Dummy driver open function called..\n");
	return 0;
}

static int dummy_release(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "Dummy driver release function called..\n");
	return 0;
}

static ssize_t dummy_read(struct file *file,char __user *buf,size_t len,loff_t *off)
{
	printk(KERN_INFO "Dummy driver read function called..\n");
	return 0;
}

static ssize_t dummy_write(struct file *file,const char *buf,size_t len,loff_t *off)
{
	printk(KERN_INFO "Dummy driver write function called..\n");
	return len;
}



static int __init dummy_init(void)
{
	printk(KERN_INFO " calling init function \n");	
	if((alloc_chrdev_region(&dev,0,1,"dummy_dev")) < 0){
		printk(KERN_INFO "cannot allocate major number\n");
	}

	/*creating cdev structure */
	
	cdev_init(&dummy_cdev,&dummy_fops);
	dummy_cdev.owner = THIS_MODULE;
	dummy_cdev.ops   = &dummy_fops;

	if((cdev_add(&dummy_cdev,dev,1)) < 0){
		printk(KERN_INFO " cannot add device to the system\n");	
		goto r_class;
	}

	if((dummy_dev_class = class_create(THIS_MODULE,"dummy_class")) < 0){	
		printk(KERN_INFO " cannot add device to the system\n");	
		goto r_class;
	}	
	
	if((device_create(dummy_dev_class,NULL,dev,NULL,"dummy_device")) < 0){
		printk(KERN_INFO " failed to create device \n");	
		goto r_device;
	}
	
	return 0;
r_device:
	class_destroy(dummy_dev_class);

r_class:
	unregister_chrdev_region(dev,1);
	return -1;

}


static void __exit dummy_exit(void)
{

	printk(KERN_INFO " calling exit function \n");	
	device_destroy(dummy_dev_class,dev);
	class_destroy(dummy_dev_class);
	cdev_del(&dummy_cdev);
	unregister_chrdev_region(dev,1);

}



module_init(dummy_init);
module_exit(dummy_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sailesh kumar");
MODULE_DESCRIPTION("A dummy char driver");
MODULE_VERSION("1.0");

