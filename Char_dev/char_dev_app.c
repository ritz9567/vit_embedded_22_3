#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdbool.h>
#include<unistd.h>

//#define Char_dev "/dev/simple_chardev"
#define Char_dev "/dev/memory_chardev"

int main()
{
   int fp; 
    fp= open(Char_dev,O_RDWR);
    char write_buff[100],read_buff[100];
    char ch;
    char dummy;
	int running=true;

    if(fp >0)
    {
        printf("Choose the operation\n R: Read\n W:Write\n E:Exit\n");
        scanf("%c",&ch);
		do{
    	    switch(ch)
        	{
            	case 'R':
                    read(fp,read_buff,sizeof(read_buff));
                    printf("%s",read_buff);
                    break;

            	case 'W':
                    printf("Enter the data to be Writeen into char driver:");
                    scanf("%c",&dummy);
                    scanf("%[^\n]",write_buff);
                    write(fp,write_buff,sizeof(write_buff));
                    break;
     			case 'E' :
					running=false;   
        
                    //printf("Invalid Option\n");
                    
        	}
    	close(fp);
		}
	while(running);
	}
	else{
		printf("unable to open char dev\n");
		exit(0);
	}
    
    return 0;
}
