/* Necessary includes for device drivers */

#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/errno.h> /* error codes */
#include <linux/types.h> /* size_t */
#include <linux/proc_fs.h>
#include <linux/cdev.h> /*character driver makes cdev available*/
#include <linux/fcntl.h> /* O_ACCMODE */
#include <linux/uaccess.h>
//#include <asm/uaccess.h> /* copy_from/to_user */

/* Micro */
#define DEVICE_NAME "memory_chardev"

/* Global Variable of the driver*/

/*Major Number */
int memory_chardev_major;

struct cdev *mychardev;
dev_t dev_num; //will hold the major number that kernel gives
int result;

/* Buffer to store Data */

char *memory_buffer;
/*Function Declaration*/
static int char_drv_init(void);
static void char_drv_exit(void);

/* Function Defination */

int char_open(struct inode *inode,struct file *filp)
{
	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
	return 0;
}

int char_release(struct inode *inode,struct file *flip)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	return 0;
}

ssize_t char_read(struct file *flip, char *buf ,size_t count, loff_t *f_pos)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	result=copy_to_user(buf,memory_buffer,count);
    	return result;

}

ssize_t char_write(struct file *flip, const char *buf, size_t count ,loff_t *f_pos)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	result=copy_from_user(memory_buffer,buf,count);
    	return result;
}


/* structure that declares the usual file */
/* access functions */

struct file_operations memory_fops ={
    	read:   char_read,
    	write:  char_write,
    	open:   char_open,
    	release:char_release,
};


/*char driver init function defination */
static int __init char_drv_init(void) /* Constructor */
{

/* Registering Device */

//alloc_chardev_region(dev_t* , uint fminor ,uint count ,chr *name);
	result=alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);

//result=register_chrdev(memory_major,DEVICE_NAME,&memory_fops);
   	if(result<0)
    	{	
        	printk(KERN_DEBUG "[WORKSHOP]: Failed to allocate major number\n");
        	return result;
    	}

	memory_chardev_major=MAJOR(dev_num);
	printk(KERN_DEBUG "[WORKSHOP]: allocated major number for %s driver is %d\n",DEVICE_NAME,memory_chardev_major);
	mychardev = cdev_alloc(); //create our cdev structure, initialized our cdev
	mychardev->ops= &memory_fops; //struct file operation
	result=cdev_add(mychardev,dev_num,1);
	if(result<0)
    	{
        	printk(KERN_DEBUG "[WORKSHOP]: unable to register the device\n");
        	return result;
    	}
/* Allocating memory for the buffer */
/*
    void * kmalloc(size_t size, int flags)
*/
	memory_buffer=kmalloc(1,GFP_KERNEL);
    	if(!memory_buffer)
    	{
        	result=-ENOMEM;
        	char_drv_exit();
        	return result;
    	}
    	else
    	{   
        
        /*void *memset(void *str, int c, size_t n) // copies the character c (an unsigned char) to the first n characters of the string pointed to, by the argument str.*/
    
        memset(memory_buffer,0,1);
	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
	}
        return 0;
}

static void __exit char_drv_exit(void) /* Destructor */
{
/*memory exit function defination */

/* Freeing the major number */
	cdev_del(mychardev);
	unregister_chrdev(memory_chardev_major,DEVICE_NAME);

/* Freeing the buffer memory */
    	if(memory_buffer)
    	{
        	kfree(memory_buffer);
    	}

	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);

}

/* Declartion of init & exit function */
module_init(char_drv_init);
module_exit(char_drv_exit);

// Information about this module:

MODULE_AUTHOR("Ritesh_D");
MODULE_DESCRIPTION("A simple character driver");
MODULE_LICENSE("GPL"); // Important to leave as GPL.
