# README #

This README would normally document whatever steps are necessary to get your application up and running.

* Quick summary
	This repo is intended to get a basic idea of device drivers

### How do I get set up? ###

* Summary of set up
	1. Run make command to compile
	2. insmod <driver_name>.ko to load a module into the kernel space
	3. rmmod <driver_name> to unload a module from the kernel space
	4. dmesg to view the kernel logs
	5. to create node use "mknod -m <permission> /dev/<driver_name> c <majorno.> <minorno.>" command
	6. to remove the node use rm /dev/<driver_name>
	7. to see the created node use ls -l /dev/<driver_name>
	8. see the comtinuous running log by tail -f /var/log/syslog
* Configuration
	modinfo <driver_name> to see basic information about driver module
