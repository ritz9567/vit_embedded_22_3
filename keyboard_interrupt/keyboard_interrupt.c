/* Necessary includes for device drivers */

#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/cdev.h> /*character driver makes cdev available*/
#include <linux/fcntl.h> /* O_ACCMODE */
#include <linux/uaccess.h>

#include<linux/interrupt.h>
#include<asm/io.h>

/* Micro */
#define DEVICE_NAME "keyboard_intrpt_module"

/* Global Variable of the driver*/

/*Major Number */
int memory_chardev_major;

struct cdev *mychardev;
dev_t dev_num; //will hold the major number that kernel gives
int result;

/* Buffer to store Data */

char *memory_buffer;

/*Function Prototype*/
static int keyboard_init(void);
static void keyboard_exit(void);

static irqreturn_t keyboard_irq_handler(int irq, void *dev)
{
        unsigned char scancode = 0;
        unsigned char status = 0;
        status   = inb(0x64);
        scancode = inb(0x60);
        switch (scancode)
        {
                case 0x01:
                        printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed Esc ...\n", __func__, __LINE__);
                break;
                case 0x3B:
                        printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed F1 ...\n", __func__, __LINE__);
                break;
                case 0x3C:
                        printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed F2 ...\n", __func__, __LINE__);
                break;
		case 0x2:
			printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed 1 ...\n", __func__, __LINE__);
		break;
		case 0x3:
			printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed 2 ...\n", __func__, __LINE__);
		break;
		case 0x4:
			printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed 3 ...\n", __func__, __LINE__);
		break;
		case 0x5:
			printk(KERN_DEBUG"[WORKSHOP]: %s:%d ! You pressed 4 ...\n", __func__, __LINE__);
		break;

                default:
		break;
        }
        return IRQ_HANDLED;
}

/* Function Defination */

int char_open(struct inode *inode,struct file *filp)
{
	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
	return 0;
}

int char_release(struct inode *inode,struct file *flip)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	return 0;
}

ssize_t char_read(struct file *flip, char *buf ,size_t count, loff_t *f_pos)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	result=copy_to_user(buf,memory_buffer,count);
    	return result;

}

ssize_t char_write(struct file *flip, const char *buf, size_t count ,loff_t *f_pos)
{
    	printk(KERN_DEBUG "[WORKSHOP]: inside %s function\n",__FUNCTION__);
    	result=copy_from_user(memory_buffer,buf,count);
    	return result;
}

/* structure that declares the usual file */
/* access functions */

struct file_operations memory_fops ={
    	read:   char_read,
    	write:  char_write,
    	open:   char_open,
    	release:char_release,
};


/*char driver init function defination */
static int keyboard_init(void) /* Constructor */
{

/* Registering Device */

	result=alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);

   	if(result<0)
    	{	
        	printk(KERN_DEBUG "[WORKSHOP]: Failed to allocate major number\n");
        	return result;
    	}

	memory_chardev_major=MAJOR(dev_num);
	printk(KERN_DEBUG "[WORKSHOP]: allocated major number for %s driver is %d\n",DEVICE_NAME,memory_chardev_major);
	mychardev = cdev_alloc(); //create our cdev structure, initialized our cdev
	mychardev->ops= &memory_fops; //struct file operation
	result=cdev_add(mychardev,dev_num,1);
	if(result<0)
    	{
        	printk(KERN_DEBUG "[WORKSHOP]: unable to register the device\n");
        	return result;
    	}

	/* Allocating memory for the buffer 
	 * (void *) kmalloc(size_t size, int flags)
	*/

	memory_buffer=kmalloc(1,GFP_KERNEL);
    	if(!memory_buffer)
    	{
        	result=-ENOMEM;
        	keyboard_exit();
        	return result;
    	}
    	else
    	{   
        
        /*void *memset(void *str, int c, size_t n) // copies the character c (an unsigned char) to the first n characters of the string pointed to, by the argument str.*/
    
        memset(memory_buffer,0,1);
	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
	}
	request_irq(1, keyboard_irq_handler, IRQF_SHARED, "keyboard_device", /*my_dev*/ mychardev);
        return 0;
}

static void keyboard_exit(void) /* Destructor */
{
/*memory exit function defination */
	free_irq(1, mychardev);
/* Freeing the major number */
	cdev_del(mychardev);
	unregister_chrdev(memory_chardev_major,DEVICE_NAME);

/* Freeing the buffer memory */
    	if(memory_buffer)
    	{
        	kfree(memory_buffer);
    	}

	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);

}

/* Declartion of init & exit function */
module_init(keyboard_init);
module_exit(keyboard_exit);

// Information about this module:

MODULE_AUTHOR("Ritesh_D");
MODULE_DESCRIPTION("A simple character driver");
MODULE_LICENSE("GPL"); // Important to leave as GPL.
