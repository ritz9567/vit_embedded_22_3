#include <linux/module.h>
#include <linux/kernel.h>

static int __init drv_init(void) /* Constructor */
{
	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
	return 0;
}

static void __exit drv_exit(void) /* Destructor */
{
	printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);
}

module_init(drv_init);
module_exit(drv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIT-Workshop");
MODULE_DESCRIPTION("Driver Template");
