# README #

This README would normally document whatever steps are necessary to get your application up and running.

* Quick summary
	This repo is intended to get a basic idea of device drivers

### Environment setup ###

* Summary of set up
```console
	1. Run make command to compile
	2. insmod <driver_name>.ko to load a module into the kernel space
	3. rmmod <driver_name> to unload a module from the kernel space
	4. dmesg to view the kernel logs
* Configuration
	modinfo <driver_name> to see basic information about driver module
```