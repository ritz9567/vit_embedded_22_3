#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/moduleparam.h>

int value,value_arr[4];
char *name;
int cb_value = 0;

module_param(value,int,S_IRUSR|S_IWUSR);
module_param(name,charp,S_IRUSR|S_IWUSR);
module_param_array(value_arr,int,NULL,S_IRUSR|S_IWUSR);


int notify_param(const char *val,const struct kernel_param *kp)
{
	int res = param_set_int(val,kp);
	if(res == 0)
	{
		printk(KERN_INFO "callback function called\n");
		printk(KERN_INFO "the new value of cb_value =%d\n",cb_value);
		printk(KERN_INFO "the value =%d\n",value);
		printk(KERN_INFO "the name  =%s\n",name);
		return 0;
	}
	return -1;
}
	


const struct kernel_param_ops my_param_ops =
{
	.set = &notify_param,
	.get = &param_get_int,
};

module_param_cb(cb_value,&my_param_ops,&cb_value,S_IRUGO|S_IWUSR);

static int __init hello_world_init(void)
{
	int i=0;
	printk(KERN_INFO "the value =%d\n",value);
	printk(KERN_INFO "the cb_value  =%d\n",cb_value);
	printk(KERN_INFO "the name  =%s\n",name);
	for(i = 0; i<(sizeof(value_arr)/sizeof(int));i++)
	{
		printk(KERN_INFO "the %d of value_arr  =%d\n",i,value_arr[i]);

	}
	printk(KERN_INFO "hello_world module inserted\n");
	return 0;
}

void __exit hello_world_exit(void)
{
	printk(KERN_INFO "hello_world module exited\n");
}

module_init(hello_world_init);
module_exit(hello_world_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sailesh kumar");
MODULE_DESCRIPTION("a simpple hello world module");

		
