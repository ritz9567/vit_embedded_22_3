#include <linux/module.h>
#include <linux/kernel.h>
#include<linux/moduleparam.h>//header file for module parameter

int count =1;
module_param(count,int,0644);//module_param(parameter name,datatype,permission)

static int __init module_param_init(void) /* Constructor */
{
	int index;
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
	for(index=0;index<count;index++)
	        printk(KERN_INFO "[WORKSHOP]: %s:%d Hello World Index=%d \n",__func__, __LINE__,index);
        return 0;
}

static void __exit module_param_exit(void) /* Destructor */
{
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);
}

module_init(module_param_init);
module_exit(module_param_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIT-Workshop");
MODULE_DESCRIPTION("Module parameter Passing");
