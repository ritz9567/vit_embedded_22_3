#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>


static int __init hello_world_init(void)
{
	printk(KERN_INFO "hello_world module inserted\n");
	return 0;
}

void __exit hello_world_exit(void)
{
	printk(KERN_INFO "hello_world module exited\n");
}

module_init(hello_world_init);
module_exit(hello_world_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sailesh kumar");
MODULE_DESCRIPTION("a simpple hello world module");

		
