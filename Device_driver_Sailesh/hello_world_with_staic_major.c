#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/fs.h>


dev_t dev = MKDEV(235,0);

static int __init hello_world_init(void)
{
	printk(KERN_INFO "hello_world module inserted\n");
	register_chrdev_region(dev,1,"hello_world_static_major");
	printk(KERN_INFO "Major=%d Minor=%d\n",MAJOR(dev),MINOR(dev));
	return 0;
}

void __exit hello_world_exit(void)
{
	unregister_chrdev_region(dev, 1);
	printk(KERN_INFO "hello_world module exited\n");
}

module_init(hello_world_init);
module_exit(hello_world_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sailesh kumar");
MODULE_DESCRIPTION("a simpple hello world module with static major number allocation");

		
