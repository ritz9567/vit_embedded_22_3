#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define WR_VALUE _IOW('a','a',int32_t*)
#define RD_VALUE _IOR('a','b',int32_t*)

int main()
{
	int fd;
	int32_t value,number;
	printf("**********************************************\n");
	printf("*********driver ioctl application**************\n");

	fd = open("/dev/real_device",O_RDWR);
	if(fd < 0){
		printf("cannot open device file \n");
		return 0;
	}	

	printf("enter data to be send \n");
	scanf("%d",&number);

	printf("writing to driver \n");
	ioctl(fd, WR_VALUE,(int32_t *)&number);

	printf("reading from driver \n");
	ioctl(fd, RD_VALUE,(int32_t *)&value);
	printf("the data is %d \n",value);

	printf("closing driver \n");

	close(fd);
}	
