#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/fs.h>
#include<linux/kdev_t.h>
#include<linux/device.h>


dev_t dev = 0;
static struct class *dev_class;

static int __init hello_world_init(void)
{
	printk(KERN_INFO "hello_world module inserted\n");
	if ((alloc_chrdev_region(&dev,0,1,"hello_world_dynamic_major_dynamic_mknod")) < 0)
		printk(KERN_INFO "alloc chrdev failed\n");

	printk(KERN_INFO "Major=%d Minor=%d\n",MAJOR(dev),MINOR(dev));

	if((dev_class = class_create(THIS_MODULE,"hello_world_dynamic_major_dynamic_mknod_class")) == NULL)
	{
		printk(KERN_INFO "class creation failed\n");
		goto r_class;
	}
	if((device_create(dev_class,NULL,dev,NULL,"hello_world_dynamic_major_dynamic_mknod_device")) == NULL)
	{
		printk(KERN_INFO "device creation failed\n");
		goto r_device;
	}
	return 0;

r_device:
	class_destroy(dev_class);
r_class:
	unregister_chrdev_region(dev, 1);
	return -1;

}

void __exit hello_world_exit(void)
{
	device_destroy(dev_class,dev);
	class_destroy(dev_class);
	unregister_chrdev_region(dev, 1);
	printk(KERN_INFO "hello_world module exited\n");
}

module_init(hello_world_init);
module_exit(hello_world_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sailesh kumar");
MODULE_DESCRIPTION("a simpple hello world module with static major number allocation");

		
