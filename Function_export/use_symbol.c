//Header files
#include<linux/kernel.h>
#include<linux/module.h>

int exported_function(void);
static int __init use_drv_init(void) /* Constructor */
{
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
	exported_function();
        return 0;
}

static void __exit use_drv_exit(void) /* Destructor */
{
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);
}

module_init(use_drv_init);
module_exit(use_drv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIT-Workshop");
MODULE_DESCRIPTION("Using exported Function");

