//Header files
#include<linux/kernel.h>
#include<linux/module.h>

static int exported_function(void)
{
    printk(KERN_DEBUG "[WORKSHOP]: Inside %s \n", __func__);
    return 0;
}

//Exporting one funnction to available in other programs
EXPORT_SYMBOL(exported_function);

static int __init export_drv_init(void) /* Constructor */
{
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver Loaded\n", __func__, __LINE__);
        return 0;
}

static void __exit export_drv_exit(void) /* Destructor */
{
        printk(KERN_DEBUG "[WORKSHOP]: %s:%d Driver unloaded\n", __func__, __LINE__);
}

module_init(export_drv_init);
module_exit(export_drv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIT-Workshop");
MODULE_DESCRIPTION("Exporting function");
